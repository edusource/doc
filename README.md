What you want is probably in the [Wiki](https://gitlab.com/edusource/doc/wikis/home). However....

# Documentation under source control

## Diagrams

* Git state diagram 
    [EPS](diagrams/git_states.eps),
    [Graffle](diagrams/git_states.graffle),
    [PDF](diagrams/git_states.pdf),
    [PNG](diagrams/git_states.png) 

## WikiCodeSamples
* MakingRestApiCalls
    [Part1](WikiCodeSamples/MakingRestApiCalls-Part1.zip),
    [Part2](WikiCodeSamples/MakingRestApiCalls-Part2.zip),
    [Part3](WikiCodeSamples/MakingRestApiCalls-Part3.zip)